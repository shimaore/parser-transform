%x pattern set tokenizer qualifier

name      [a-zA-Z]+[a-zA-Z0-9]*
hexa      [0-9a-fA-F]
hexa4     {hexa}{hexa}{hexa}{hexa}

%%

{name}\s+                         this.popState(); this.begin('pattern'); yytext = yytext.trim(); return 'NAME'
\%\%.*\n                          this.popState(); this.begin('tokenizer');                       return "'%%'"

<pattern>[\t ]+\S+\n              this.popState(); yytext = yytext.slice(0,-1).trim(); return 'END_OF_LINE'
<pattern>[\t ]*\n                 this.popState(); yytext = '';                        return 'END_OF_LINE'

<tokenizer>[\t ]+.*\n   this.popState(); this.begin('tokenizer'); yytext = yytext.slice(0,-1).trim(); return 'END_OF_TOKEN'
<tokenizer>\<           this.popState(); this.begin('tokenizer'); this.begin('qualifier')

<pattern,tokenizer>[()+*?.|]                                                                    return `'${yytext[0]}'`
<pattern,tokenizer>\[             this.begin('set');                                            return 'START_SET'
<pattern,tokenizer>\{{name}\}     yytext = yytext.slice(1,-1);                                  return 'QUOTED_NAME'
<pattern,tokenizer>\\t            yytext = '\t';                                                return 'PATTERN_TOKEN'
<pattern,tokenizer>\\r            yytext = '\r';                                                return 'PATTERN_TOKEN'
<pattern,tokenizer>\\n            yytext = '\n';                                                return 'PATTERN_TOKEN'
<pattern,tokenizer>\\u{hexa4}     yytext = String.fromCodePoint(parseInt(yytext.slice(2),16));  return 'PATTERN_TOKEN'
<pattern,tokenizer>\\.            yytext = yytext.slice(1);                                     return 'PATTERN_TOKEN'
<pattern,tokenizer>.                                                                            return 'PATTERN_TOKEN'

<set>\]                 this.popState();                                              return 'END_SET'
<set>\^                                                                               return `INVERT_SET`
<set>\-                                                                               return `RANGE`
<set>\\t                yytext = '\t';                                                return 'SET_TOKEN'
<set>\\r                yytext = '\r';                                                return 'SET_TOKEN'
<set>\\n                yytext = '\n';                                                return 'SET_TOKEN'
<set>\\u{hexa4}         yytext = String.fromCodePoint(parseInt(yytext.slice(2),16));  return 'SET_TOKEN'
<set>\\.                yytext = yytext.slice(1);                                     return 'SET_TOKEN'
<set>.                                                                                return 'SET_TOKEN'


<qualifier>{name}     return 'QUALIFIER_NAME'
<qualifier>[,]        /* ignore */
<qualifier>\>         this.popState()

